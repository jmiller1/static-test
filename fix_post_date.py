#posts/test1543172640.meta

import os
import re
from datetime import datetime

def fixDatetimeFormat(file_path):
    with open(file_path, 'r+') as f:
        textToSearch = f.read()
        for match in re.findall("\d\d\d\d/\d\d/\d\d \d\d:\d\d:\d\d", textToSearch):
            #convert match to new format
            datetime_object = datetime.strptime(match, "%Y/%m/%d %H:%M:%S")
            dateNewFormat = datetime_object.strftime("%Y-%m-%d %H:%M:%S")
            #substitute the old date with the new
            textToSearch = re.sub(match, dateNewFormat + " UTC-00:00", textToSearch)
            #textToSearch = re.sub(match, "2010-08-27 03:11:03", textToSearch)
        
        with open(file_path, 'w+') as f2:
            f2.write(textToSearch)
        f.close()
        f2.close()
        return True

for file_path in os.listdir("posts"):
    if file_path.endswith(".meta"):
        file_path = os.path.join("posts", file_path)
        print(file_path)
        fixDatetimeFormat(file_path)

